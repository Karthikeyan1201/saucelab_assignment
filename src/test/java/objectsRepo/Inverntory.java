package objectsRepo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeoutException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import utilities.Common_Functions;

public class Inverntory {
	
	Common_Functions function = new Common_Functions();
	
	public Inverntory(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//*[@id='add-to-cart-sauce-labs-backpack']")
	private WebElement addCardtBackPack;
	
	@FindBy(xpath = "//*[@id='remove-sauce-labs-backpack']")
	private WebElement removeCardtBackPack;
	
	@FindBy(xpath = "//span[@class='shopping_cart_badge']")
	private WebElement cartVerify;
	
	@FindBys({@FindBy(xpath = "//*[@class='inventory_item_description']/div/a/div")})
	private List<WebElement> productName;
	
	@FindBys({@FindBy(xpath = "//*[@class='inventory_item_description']/div/div")})
	private List<WebElement> productDescription;

	@FindBys({@FindBy(xpath = "//*[@class='inventory_item_description']/div[2]/div")})
	private List<WebElement> productPrice;

	@FindBy(xpath = "//*[@class='inventory_item_name']")
	private WebElement cartProdName;

	@FindBy(xpath = "//*[@class='inventory_item_price']")
	private WebElement cartProdPrice;

	@FindBy(xpath = "//*[@class='inventory_item_desc']")
	private WebElement cartProdDescription;

	@FindBy(xpath = "//*[@class='inventory_details_name large_size']")
	private WebElement prodPageName;

	@FindBy(xpath = "//*[@class='inventory_details_price']")
	private WebElement prodPagePrice;
	
	@FindBy(id = "checkout")
	private WebElement checkout;
	
	@FindBy(id = "first-name")
	private WebElement firstName;
	
	@FindBy(id = "last-name")
	private WebElement lastName;
	
	@FindBy(id = "postal-code")
	private WebElement zipCode;

	@FindBy(id = "continue")
	private WebElement continueCheckout;

	@FindBy(id = "back-to-products")
	private WebElement returnToProducts;

	@FindBy(xpath = "//h2[@class='complete-header']")
	private WebElement confirmOrderSuccess;

	@FindBy(id = "finish")
	private WebElement finishCheckout;

	public WebElement getReturnToProducts() {
		return returnToProducts;
	}

	public WebElement getProdPageName() {
		return prodPageName;
	}

	public WebElement getProdPagePrice() {
		return prodPagePrice;
	}

	public WebElement getConfirmOrderSuccess() {
		return confirmOrderSuccess;
	}

	public WebElement getFinishCheckout() {
		return finishCheckout;
	}

	public WebElement getContinueCheckout() {
		return continueCheckout;
	}

	public WebElement getFirstName() {
		return firstName;
	}

	public WebElement getLastName() {
		return lastName;
	}

	public WebElement getZipCode() {
		return zipCode;
	}

	public WebElement getCheckout() {
		return checkout;
	}

	public WebElement getAddCardtBackPack() {
		return addCardtBackPack;
	}

	public WebElement getCartVerify() {
		return cartVerify;
	}

	public List<WebElement> getProductName() {
		return productName;
	}

	public List<WebElement> getProductDescription() {
		return productDescription;
	}

	public List<WebElement> getProductPrice() {
		return productPrice;
	}

	public WebElement getCartProdName() {
		return cartProdName;
	}

	public WebElement getCartProdPrice() {
		return cartProdPrice;
	}

	public WebElement getCartProdDescription() {
		return cartProdDescription;
	}

	public WebElement getRemoveCardtBackPack() {
		return removeCardtBackPack;
	}

	public void addBackPactToCart() {
		function.click(getAddCardtBackPack());
		function.verfiyText(getRemoveCardtBackPack(), "REMOVE");
		function.verfiyText(getCartVerify(), "1");
	}

	public void checkoutProduct(WebDriver driver) throws TimeoutException{
		getCartVerify().click();
		getCheckout().click();
		function.waitForElementVisibile(driver, getFirstName());
		function.sendkeys(getFirstName(), "Karthikeyan");
		function.sendkeys(getLastName(), "Arumugam");
		function.sendkeys(getZipCode(), "560068");
		function.scrollDowntoElement(driver,getContinueCheckout());
		function.click(getContinueCheckout());
	}

	public void verifyProductDetails(WebDriver driver, Map<String,String> map) {
		function.verfiyText(getCartProdName(), map.get("productName"));
		function.verfiyText(getCartProdPrice(), map.get("productPrice"));
		function.verfiyText(getCartProdDescription(), map.get("productDescription"));
		function.scrollDowntoElement(driver,getFinishCheckout());
		function.click(getFinishCheckout());
		function.verfiyText(getConfirmOrderSuccess(), "THANK YOU FOR YOUR ORDER");
	}
	
	public Map<String,String> productDetails() {
		String productName = getProductName().get(0).getText();
		String productPrice = getProductPrice().get(0).getText();
		String productDescription = getProductDescription().get(0).getText();
		
		Map<String,String> map = new HashMap<String, String>();
		map.put("productName", productName);
		map.put("productPrice", productPrice);
		map.put("productDescription", productDescription);
		
		return map;
	}
	
	public void searchProduct(String product) {
		List<WebElement> products= getProductName();
		for(int i=0; i<products.size();i++) {
			if(getProductName().get(i).getText().equalsIgnoreCase("Sauce Labs Bolt T-Shirt")) {
				System.out.println(product + " found");
				break;
			}
		}
	}
	
	public Map<String, String> getAllProductPrice() {
		List<WebElement> priceList= getProductPrice();
		List<WebElement> productsList= getProductName();
		Map<String, String> price = new HashMap<String, String>();
		for(int i=0; i<priceList.size();i++) {
			String name = productsList.get(i).getText();
			String cost = priceList.get(i).getText();
			int len = cost.length();
			String original = cost.substring(1);
			price.put(name, original);
		}
		return price;
	}
	
	public void verifyProductPrice(WebDriver driver, Map<String, String> details) {
		
		List<WebElement> productsList= getProductName();
		int len = productsList.size();
		for(int i=0; i<len;i++) {
			String prod = productsList.get(i).getText();
			function.linkText(driver, prod);
			function.verfiyText(getProdPageName(), prod);
			String detailPagePrice = getProdPagePrice().getText().substring(1);
			Assert.assertEquals(detailPagePrice, details.get(prod),"Price doesn't match" );
			function.click(getReturnToProducts());
		}
		
	}
	
}
